﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Web;
using PostSharp.Aspects;
using log4net;

namespace MvcUserManagement.AOP
{
    [Serializable]
    public class LoggingAspect : OnMethodBoundaryAspect
    {

        public override void OnEntry(MethodExecutionArgs args)
        {
            if(args.Instance != null)
            {
                ParameterInfo[] parameters = args.Method.GetParameters();
                object[] arguments = args.Arguments.ToArray();
                var paramsWithArguments = new System.Text.StringBuilder();

                for (Int32 i = 0; i <= parameters.Length - 1; i++)
                {
                    String argument = arguments[i] != null ? arguments[i].ToString() : "null";
                    paramsWithArguments.Append(String.Format("{0}:={1}, ", parameters[i].Name, argument));
                }
                args.MethodExecutionTag = Stopwatch.StartNew();
                MDC.Set("user", HttpContext.Current.User.Identity.Name);
                MDC.Set("method", args.Method.Name);
                MDC.Set("parameters", paramsWithArguments.ToString());
                LogManager.GetLogger(args.Instance.GetType()).DebugFormat(string.Format("Enter : {0}.{1}", args.Method.DeclaringType.FullName, args.Method.Name));
            }
                
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            
            if (args.Instance != null)
            {
                MDC.Set("user", HttpContext.Current.User.Identity.Name);
                LogManager.GetLogger(args.Instance.GetType()).DebugFormat(string.Format("Success: {0}.{1}", args.Method.DeclaringType.FullName, args.Method.Name));
            }
            
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            
            if (args.Instance != null)
            {
                var sw = (Stopwatch)args.MethodExecutionTag;
                sw.Stop();
                MDC.Set("user", HttpContext.Current.User.Identity.Name);
                LogManager.GetLogger(args.Instance.GetType()).DebugFormat(string.Format("Exit: {0}.{1}", args.Method.DeclaringType.FullName, args.Method.Name));
            }
                
        }

        public override void OnException(MethodExecutionArgs args)
        {
            if (args.Instance != null)
            {
                MDC.Set("user", HttpContext.Current.User.Identity.Name);
                LogManager.GetLogger(args.Instance.GetType()).ErrorFormat(
                    string.Format("{0}.{1} Wystąpił błąd aplikacji: {2} w \n\n {3}", args.Method.DeclaringType.Name,
                                  args.Method.Name, args.Exception.GetType(), args.Exception.StackTrace), args.Exception.InnerException.ToString());
            }

        }

    
    }
}