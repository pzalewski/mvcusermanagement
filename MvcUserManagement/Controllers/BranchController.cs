﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using MvcUserManagement.Models;

namespace MvcUserManagement.Controllers
{
    public class BranchController : Controller
    {
        readonly UserRepository _repo = new UserRepository();
        //
        // GET: /Branch/

        [HttpGet]
        public ActionResult Index()
        {
            if (TempData["message"] != null)
            {
                var info = TempData["message"] as MessageInfo;
                if (info != null) ViewBag.Message = info.Message;
            }
            var lstCompanies = _repo.FindAllCompanies().ToSelectListItem(1, c => c.IdCompany, c => c.Name, c => c.Name, c => c.IdCompany.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstCompanies = lstCompanies;
            return View();
        }

        public ActionResult IsBranchNameExist(string name, int idCompany)
        {
            return Json(!_repo.IsBranchNameExist(name, idCompany), JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsBranchNoExist(int noBranch, int idCompany)
        {
            return Json(!_repo.IsBranchNoExist(noBranch, idCompany), JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsBranchNameExistBeyondId(string name, int idCompany, int idBranch)
        {
            return Json(!_repo.IsBranchNameExistBeyondId(name, idCompany, idBranch), JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult IsBranchNoExistBeyondId(int noBranch, int idCompany, int idBranch)
        {
            return Json(!_repo.IsBranchNoExistBeyondId(noBranch, idCompany, idBranch), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetBranchesList(string name, int idCompany, int toTake)
        {
            var lst = _repo.GetBranchesByNameCompany(name, idCompany, 0, toTake);
            int count = _repo.GetCountOfBranches(name, idCompany);

            int paging = 1;
            if (toTake > 0)
            {
                paging = count / toTake;
                if (count % toTake != 0)
                    paging++;
            }

            return Json(new { branchList = lst.ToArray(), count = paging });
        }

        [HttpPost]
        public ActionResult GetNextPage(string name, int idCompany, int toTake, int page)
        {

            var lst = _repo.GetBranchesByNameCompany(name, idCompany, toTake * (page - 1), toTake);
            int count = _repo.GetCountOfBranches(name, idCompany);
            int paging = count / toTake;
            if (count % toTake != 0)
                paging++;
            return Json(new { branchList = lst.ToArray(), count = paging });
        }


        public JsonResult AddJQueryBranch(string name, string noBranch, string idRegion, int idCompany, string idParent)
        {
            int tmpNoBranch;
            bool isNoBranch = int.TryParse(noBranch, out tmpNoBranch);
            if (name != String.Empty && isNoBranch)
            {
                if (_repo.IsBranchExist(name, tmpNoBranch, idCompany))
                {
                    return Json(new { IsExist = true, Message = "Taki oddział już istnieje!" });
                }
                var branch = new Branch {Name = name, NoBranch = tmpNoBranch, IdCompany = idCompany};
                int tmpIdRegion;
                if(int.TryParse(idRegion, out tmpIdRegion))
                    branch.IdRegion = tmpIdRegion;
                int tmpIdParent;
                if (int.TryParse(idParent, out tmpIdParent))
                {
                    branch.IdParent = tmpIdParent;
                    branch.IsPK = true;
                }
                else 
                {
                    branch.IsPK = false;
                }
                //ent.Jobs.AddObject(job);
                //ent.SaveChanges();
                _repo.AddBranch(branch);
                _repo.Save();

                var lstBranches = _repo.GetBranchesByCompany(idCompany).ToSelectListItem(0, b => b.IdBranch, b => b.NoBranch != null ? b.Name + " - " + b.NoBranch.ToString() : b.Name, b => b.NoBranch != null ? b.Name + " - " + b.NoBranch.ToString() : b.Name, b => b.IdBranch.ToString(CultureInfo.InvariantCulture));
                return Json(new { IsExist = false, lstJob = lstBranches.ToArray() });
            }
            var errMsg = isNoBranch ? "Proszę wpisać nazwę oddziału!" : "Proszę wpisać poprawny nr oddziału!";
            return Json(new { IsExist = true, Message = errMsg });
        }

        [HttpGet]
        public ActionResult AddBranch(int idCompany)
        {
            var lstBranches = _repo.GetBranchesByCompany(idCompany).ToSelectListItem(0, b => b.IdBranch, b => b.NoBranch != null ? b.Name + " - " + b.NoBranch.ToString() : b.Name, b => b.NoBranch != null ? b.Name + " - " + b.NoBranch.ToString() : b.Name, b => b.IdBranch.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstBranches = lstBranches;
            var lstRegions = _repo.GetRegionsByCompany(idCompany).ToSelectListItem(0, r => r.IdRegion, r => r.Name, r => r.Name, r => r.IdRegion.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstRegions = lstRegions;
            var branch = new CreateBranch {IdCompany = idCompany};
            var comp = _repo.GetCompanyById(idCompany);
            ViewBag.CompanyName = comp.Name;
            return View(branch);
        }

        [HttpPost]
        public ActionResult AddBranch(int idCompany, FormCollection formValues)
        {
            if (ModelState.IsValid)
            {
                var branch = new Branch
                                    {
                                        Name = formValues["Name"],
                                        NoBranch = Convert.ToInt32(formValues["NoBranch"]),
                                        IdCompany = Convert.ToInt32(formValues["IdCompany"])
                                    };
                int idRegion;
                if (int.TryParse(formValues["ddlRegions"], out idRegion))
                {
                    branch.IdRegion = idRegion;
                }
                if (formValues["isPK"] == "true,false")
                {
                    branch.IsPK = true;
                    branch.IdParent = Convert.ToInt32(formValues["ddlBranches"]);
                }
                else
                    branch.IsPK = false;
                _repo.AddBranch(branch);
                _repo.Save();
                TempData["message"] = new MessageInfo("Oddział " + formValues["Name"] + " - " + formValues["NoBranch"] + " został dodany.");
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditBranch(int idBranch)
        {
            Branch branch = _repo.GetBranchById(idBranch);

            var lstBranches = _repo.GetBranchesByCompany(Convert.ToInt32(branch.IdCompany)).ToSelectListItem(branch.IdParent != null ? branch.IdBranch : 0, b => b.IdBranch, b => b.NoBranch != null ? b.Name + " - " + b.NoBranch.ToString() : b.Name, b => b.NoBranch != null ? b.Name + " - " + b.NoBranch.ToString() : b.Name, b => b.IdBranch.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstBranches = lstBranches;
            var lstRegions = _repo.GetRegionsByCompany(Convert.ToInt32(branch.IdCompany)).ToSelectListItem(branch.IdRegion != null ? (int)branch.IdRegion : 0, r => r.IdRegion, r => r.Name, r => r.Name, r => r.IdRegion.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstRegions = lstRegions;
            var comp = _repo.GetCompanyById(branch.IdCompany);
            ViewBag.CompanyName = comp.Name;
            return View(branch);
        }

        [HttpPost]
        public ActionResult EditBranch(int idBranch, FormCollection formValues)
        {
            if (ModelState.IsValid)
            {
                var branch = _repo.GetBranchById(idBranch);
                branch.Name = formValues["Name"];
                branch.NoBranch = Convert.ToInt32(formValues["NoBranch"]);
                branch.IdCompany = Convert.ToInt32(formValues["IdCompany"]);
                int idRegion;
                if (int.TryParse(formValues["ddlRegions"], out idRegion))
                {
                    branch.IdRegion = idRegion;
                }
                if (formValues["isPK"] == "true,false")
                {
                    branch.IsPK = true;
                    branch.IdParent = Convert.ToInt32(formValues["ddlBranches"]);
                }
                else
                {
                    branch.IsPK = false;
                    branch.IdParent = null;
                }
                _repo.Save();
                TempData["message"] = new MessageInfo("Oddział " + formValues["Name"] + " - " + formValues["NoBranch"] + " został zmieniony.");
            }
            return RedirectToAction("Index");
        }
    }
}
