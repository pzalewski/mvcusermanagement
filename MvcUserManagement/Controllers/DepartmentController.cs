﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using MvcUserManagement.Models;

namespace MvcUserManagement.Controllers
{
    public class DepartmentController : Controller
    {
        readonly UserRepository _repo = new UserRepository();

        //
        // GET: /Department/

        public ActionResult Index()
        {
            if (TempData["message"] != null)
            {
                var info = TempData["message"] as MessageInfo;
                if (info != null) ViewBag.Message = info.Message;
            }
            IEnumerable<SelectListItem> lstCompanies = _repo.FindAllCompanies().ToSelectListItem(1, c => c.IdCompany, c => c.Name, c => c.Name, c => c.IdCompany.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstCompanies = lstCompanies;
            return View();
        }

        public ActionResult IsDepartmentExist(string name, int idCompany)
        {
            return Json(!_repo.IsDepartmentExist(name, idCompany), JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsDepartmentExistBeyondId(string name, int idCompany, int idDepartment)
        {
            return Json(!_repo.IsDepartmentExistBeyondId(name, idCompany, idDepartment), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDepartmentsList(string name, int idCompany, int toTake)
        {
            var lst = _repo.GetDepartmentsByNameCompany(name, idCompany, 0, toTake);
            int count = _repo.GetCountOfDepartments(name, idCompany);

            int paging = 1;
            if (toTake > 0)
            {
                paging = count / toTake;
                if (count % toTake != 0)
                    paging++;
            }

            return Json(new { departmentList = lst.ToArray(), count = paging });
        }

        [HttpPost]
        public ActionResult GetNextPage(string name, int idCompany, int toTake, int page)
        {

            var lst = _repo.GetDepartmentsByNameCompany(name, idCompany, toTake * (page - 1), toTake);
            int count = _repo.GetCountOfDepartments(name, idCompany);
            int paging = count / toTake;
            if (count % toTake != 0)
                paging++;
            return Json(new { departmentList = lst.ToArray(), count = paging });
        }

        [HttpPost]
        public JsonResult AddJQueryDepartment(string name, int idCompany)
        {
            if (name != String.Empty)
            {
                if (_repo.IsDepartmentExist(name, idCompany))
                {
                    return Json(new { IsExist = true, Message = "Taki dział już istnieje!" });
                }
                var dep = new Department {Name = name, IdCompany = idCompany};
                //ent.Jobs.AddObject(job);
                //ent.SaveChanges();
                _repo.AddDepartment(dep);
                _repo.Save();

                IEnumerable<SelectListItem> lstDepartments = _repo.GetDepartmentsByCompany(idCompany).ToSelectListItem(0, d => d.IdDepartment, d => d.Name, d => d.Name, d => d.IdDepartment.ToString(CultureInfo.InvariantCulture));
                return Json(new { IsExist = false, lstDep = lstDepartments.ToArray() });
            }
            return Json(new { IsExist = true, Message = "Proszę wpisać nazwę działu!" });
        }

        [HttpGet]
        public ActionResult AddDepartment(int idCompany)
        {
            var department = new CreateDepartment {IdCompany = idCompany};
            Company comp = _repo.GetCompanyById(idCompany);
            ViewBag.CompanyName = comp.Name;
            return View(department);
        }

        [HttpPost]
        public ActionResult AddDepartment(int idCompany, FormCollection formValues)
        {
            if (ModelState.IsValid)
            {
                var department = new Department
                                     {Name = formValues["Name"], IdCompany = Convert.ToInt32(formValues["IdCompany"])};
                _repo.AddDepartment(department);
                _repo.Save();
                TempData["message"] = new MessageInfo("Jednostka organizacyjna " + formValues["Name"] + " została dodana.");
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditDepartment(int idDepartment)
        {
            Department department = _repo.GetDepartmentById(idDepartment);
            return View(department);
        }

        [HttpPost]
        public ActionResult EditDepartment(int idDepartment, FormCollection formValues)
        {
            if (ModelState.IsValid)
            {
                Department department = _repo.GetDepartmentById(idDepartment);
                department.Name = formValues["Name"];
                department.IsActive = formValues["isActive"] == "true,false";
                _repo.Save();
                TempData["message"] = new MessageInfo("Jednostka organizacyjna " + formValues["Name"] + " została zmieniona.");
            }
            return RedirectToAction("Index");
        }

    }
}
