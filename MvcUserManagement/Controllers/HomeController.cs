﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using MvcUserManagement.Models;


namespace MvcUserManagement.Controllers
{
    public class HomeController : Controller
    {
        readonly UserRepository _repo = new UserRepository();
        
        //
        // GET: /Home/

        public ActionResult AsyncRoles(int idApplication)
        {
            var roles = _repo.GetAllRolesByApp(idApplication).ToSelectListItem(0, r => r.IdRole, r => r.Name,
                                                                               r => r.Name,
                                                                               r =>
                                                                               r.IdRole.ToString(
                                                                                   CultureInfo.InvariantCulture));
            return Json(roles);
        }

        public ActionResult DeleteUserFromRole(int idUserRole)
        {
            _repo.DeleteUserRole(idUserRole);
            _repo.Save();
            return Json("OK");
        }

        [HttpPost]
        public JsonResult AddUserToRole(int idRole, int idUser)
        {
            if(!_repo.IsUserInRole(idUser, idRole))
            {
                var role = new UserRole {IdUser = idUser, IdRole = idRole, FromDate = DateTime.Now};
                _repo.AddUserRole(role);
                _repo.Save();
                return Json(new {roleId = role.IdUserRole, roleName = role.Role.Name, appName = role.Role.Application.Name, date = role.FromDate.ToString(), isOK = true });
            }
            return Json(new { isOK = false });
        }

        [HttpGet]
        public ActionResult Index()
        {
            if (TempData["message"] != null)
            {
                var info = TempData["message"] as MessageInfo;
                if (info != null) ViewBag.Message = info.Message;
            }
            IEnumerable<SelectListItem> lstCompanies = _repo.FindAllCompanies().ToSelectListItem(1, c => c.IdCompany, c => c.Name, c => c.Name, c => c.IdCompany.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstCompanies = lstCompanies;
            return View();
        }

        [HttpPost]
        public ActionResult GetUsersList(string name, int idCompany, int toTake)
        {
            var lst = _repo.GetUserByLNameCompany(name, idCompany, 0, toTake);
            int count = _repo.GetCountOfUsers(name, idCompany);

            int paging = 1;
            if (toTake > 0)
            {
                paging = count / toTake;
                if (count % toTake != 0)
                    paging++;
            }
            
            return Json(new { userList = lst.ToArray(), count = paging });
        }

        [HttpPost]
        public ActionResult GetNextPage(string name, int idCompany, int toTake, int page)
        {

            var lst = _repo.GetUserByLNameCompany(name, idCompany, toTake * (page - 1), toTake);
            int count = _repo.GetCountOfUsers(name, idCompany);
            int paging = count / toTake;
            if (count % toTake != 0)
                paging++;
            return Json(new { userList = lst.ToArray(), count = paging });
        }

        [HttpGet]
        public ActionResult EditUser(int idUser) 
        {
            var u = _repo.GetUserById(idUser);
            IEnumerable<SelectListItem> lstJobs =
                _repo.GetJobsByCompany(Convert.ToInt32(u.IdCompany)).ToSelectListItem(
                    u.IdJob != null ? (int) u.IdJob : 0, j => j.IdJob, j => j.Name, j => j.Name,
                    j => j.IdJob.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstJobs = lstJobs;
            IEnumerable<SelectListItem> lstBranches =
                _repo.GetBranchesByCompany(Convert.ToInt32(u.IdCompany)).ToSelectListItem(u.IdBranch != null ? (int) u.IdBranch : 0, b => b.IdBranch,
                                                                                                                                     b => b.NoBranch != null ? b.Name + " - " + b.NoBranch.ToString() : b.Name,
                                                                                                                                     b => b.NoBranch != null ? b.Name + " - " + b.NoBranch.ToString() : b.Name,
                                                                                                                                     b => b.IdBranch.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstBranches = lstBranches;
            IEnumerable<SelectListItem> lstDepartments =
                _repo.GetDepartmentsByCompany(Convert.ToInt32(u.IdCompany)).ToSelectListItem(u.IdDepartment != null ? (int) u.IdDepartment : 0, d => d.IdDepartment, 
                                                                                                                                                d => d.Name, d => d.Name,
                                                                                                                                                d => d.IdDepartment.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstDepartments = lstDepartments;
            IEnumerable<SelectListItem> lstManagers =
                _repo.GetManagersByCompany(Convert.ToInt32(u.IdCompany)).ToSelectListItem(u.IdManager != null ? (int) u.IdManager : 0, m => m.IdUser, 
                                                                                                                                       m => m.LastName + " " + m.FirstName,
                                                                                                                                       m => m.LastName + " " + m.FirstName, m => m.IdUser.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstManagers = lstManagers;
            IEnumerable<SelectListItem> lstRegions =
                _repo.GetRegionsByCompany(Convert.ToInt32(u.IdCompany)).ToSelectListItem(0, r => r.IdRegion, r => r.Name,
                                                                                         r => r.Name,
                                                                                         r =>
                                                                                         r.IdRegion.ToString(
                                                                                             CultureInfo.
                                                                                                 InvariantCulture));
            ViewBag.lstRegions = lstRegions;
            IEnumerable<SelectListItem> lstApplications = 
                _repo.FindAllAplications().ToSelectListItem(0,
                                                            a =>
                                                            a.IdApplication,
                                                            a => a.Name,
                                                            a => a.Name,
                                                            a =>
                                                            a.IdApplication.
                                                                ToString(
                                                                    CultureInfo
                                                                        .
                                                                        InvariantCulture));
            ViewBag.lstApplications = lstApplications;

            ViewBag.lstUserRoles = _repo.GetAllRolesByUser(Convert.ToInt32(u.IdUser));

            return View(u);
        }

        [HttpPost]
        public ActionResult EditUser(int idUser, string FirstName, string LastName, string ddlJobs, string ddlBranches, string ddlDepartments, string ddlManagers, string Email, string isActive)
        {
            var u = _repo.GetUserById(idUser);
            u.FirstName = FirstName;
            u.LastName = LastName;
            if(ddlJobs != "")
                u.IdJob = Convert.ToInt32(ddlJobs);
            if (ddlBranches != "")
                u.IdBranch = Convert.ToInt32(ddlBranches);
            if (ddlDepartments != "")
                u.IdDepartment = Convert.ToInt32(ddlDepartments);
            if (ddlManagers != "")
                u.IdManager = Convert.ToInt32(ddlManagers);
            u.Email = Email;
            u.IsActive = isActive == "true,false" || isActive == "true";
            _repo.Save();
            TempData["message"] = new MessageInfo("Użytkownik " + LastName + " " + FirstName + " został zmieniony.");
            return RedirectToAction("Index");
        }

        //[HttpPost]
        //public ActionResult EditUser(int idUser, FormCollection formValues)
        //{
        //    var u = _repo.GetUserById(idUser);
        //    u.FirstName = formValues["FirstName"];
        //    u.LastName = formValues["LastName"];
        //    if (formValues["ddlJobs"] != "")
        //        u.IdJob = Convert.ToInt32(formValues["ddlJobs"]);
        //    if (formValues["ddlBranches"] != "")
        //        u.IdBranch = Convert.ToInt32(formValues["ddlBranches"]);
        //    if (formValues["ddlDepartments"] != "")
        //        u.IdDepartment = Convert.ToInt32(formValues["ddlDepartments"]);
        //    if (formValues["ddlManagers"] != "")
        //        u.IdManager = Convert.ToInt32(formValues["ddlManagers"]);
        //    u.Email = formValues["Email"];
        //    u.IsActive = formValues["isActive"] == "true,false";
        //    _repo.Save();
        //    TempData["message"] = new MessageInfo("Użytkownik " + formValues["LastName"] + " " + formValues["FirstName"] + " został zmieniony.");
        //    return RedirectToAction("Index");
        //}

        [HttpPost]
        public ActionResult CanAddUser(int idCompany)
        {
            bool canAdd = !_repo.IsImported(idCompany);
            return Json(new { responce = canAdd });
        }

        [HttpGet]
        public ActionResult AddUser(int idCompany)
        {
            IEnumerable<SelectListItem> lstJobs = _repo.GetJobsByCompany(idCompany).ToSelectListItem(0, j => j.IdJob,
                                                                                                     j => j.Name,
                                                                                                     j => j.Name,
                                                                                                     j =>
                                                                                                     j.IdJob.ToString(
                                                                                                         CultureInfo.
                                                                                                             InvariantCulture));
            ViewBag.lstJobs = lstJobs;
            IEnumerable<SelectListItem> lstBranches = _repo.GetBranchesByCompany(idCompany).ToSelectListItem(0, b => b.IdBranch,
                                                                                                                b => b.NoBranch != null ? b.Name + " - " + b.NoBranch.ToString() : b.Name, 
                                                                                                                b => b.NoBranch != null ? b.Name + " - " + b.NoBranch.ToString() : b.Name, 
                                                                                                                b => b.IdBranch.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstBranches = lstBranches;
            IEnumerable<SelectListItem> lstDepartments = _repo.GetDepartmentsByCompany(idCompany).ToSelectListItem(0, d => d.IdDepartment, 
                                                                                                                      d => d.Name, d => d.Name, 
                                                                                                                      d => d.IdDepartment.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstDepartments = lstDepartments;
            IEnumerable<SelectListItem> lstManagers = _repo.GetManagersByCompany(idCompany).ToSelectListItem(0, m => m.IdUser, 
                                                                                                                m => m.LastName + " " + m.FirstName, 
                                                                                                                m => m.LastName + " " + m.FirstName, 
                                                                                                                m => m.IdUser.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstManagers = lstManagers;
            IEnumerable<SelectListItem> lstRegions = _repo.GetRegionsByCompany(idCompany).ToSelectListItem(0, r => r.IdRegion, 
                                                                                                              r => r.Name, 
                                                                                                              r => r.Name, 
                                                                                                              r => r.IdRegion.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstRegions = lstRegions;
            var u = new User {IdCompany = idCompany, Company = _repo.GetCompanyById(idCompany)};
            return View(u);
        }

        [HttpPost]
        public ActionResult AddUser(int idCompany, FormCollection formValues) 
        {
            var u = new User {FirstName = formValues["FirstName"], LastName = formValues["LastName"]};
            if (formValues["ddlJobs"] != "")
                u.IdJob = Convert.ToInt32(formValues["ddlJobs"]);
            if (formValues["ddlBranches"] != "")
                u.IdBranch = Convert.ToInt32(formValues["ddlBranches"]);
            if (formValues["ddlDepartments"] != "")
                u.IdDepartment = Convert.ToInt32(formValues["ddlDepartments"]);
            if (formValues["ddlManagers"] != "")
                u.IdManager = Convert.ToInt32(formValues["ddlManagers"]);
            u.Email = formValues["Email"];
            u.IsActive = formValues["isActive"] == "true,false";
            u.IdCompany = idCompany;
            _repo.AddUser(u);
            _repo.Save();
            TempData["message"] = new MessageInfo("Użytkownik " + formValues["LastName"] + " " + formValues["FirstName"] + " został dodany.");
            return RedirectToAction("Index");
        }
    }
}
