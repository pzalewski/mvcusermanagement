﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using MvcUserManagement.Models;

namespace MvcUserManagement.Controllers
{
    public class JobController : Controller
    {
        readonly UserRepository _repo = new UserRepository();
        //
        // GET: /Job/

        [HttpGet]
        public ActionResult Index()
        {
            if (TempData["message"] != null)
            {
                var info = TempData["message"] as MessageInfo;
                if (info != null) ViewBag.Message = info.Message;
            }
            var lstCompanies = _repo.FindAllCompanies().ToSelectListItem(1, c => c.IdCompany, c => c.Name, c => c.Name, c => c.IdCompany.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstCompanies = lstCompanies;
            return View();
        }

        [HttpPost]
        public ActionResult GetJobsList(string name, int idCompany, int toTake)
        {
            var lst = _repo.GetJobsByNameCompany(name, idCompany, 0, toTake);
            int count = _repo.GetCountOfJobs(name, idCompany);

            int paging = 1;
            if (toTake > 0)
            {
                paging = count / toTake;
                if (count % toTake != 0)
                    paging++;
            }

            return Json(new { jobList = lst.ToArray(), count = paging });
        }

        [HttpPost]
        public ActionResult GetNextPage(string name, int idCompany, int toTake, int page)
        {

            var lst = _repo.GetJobsByNameCompany(name, idCompany, toTake * (page - 1), toTake);
            int count = _repo.GetCountOfJobs(name, idCompany);
            int paging = count / toTake;
            if (count % toTake != 0)
                paging++;
            return Json(new { jobList = lst.ToArray(), count = paging });
        }

        public ActionResult IsJobExist(string name, int idCompany)
        {
            return Json(!_repo.IsJobExist(name, idCompany), JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsJobExistBeyondId(string name, int idCompany, int idJob)
        {
            return Json(!_repo.IsJobExistBeyondId(name, idCompany, idJob), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddJQueryJob(string name, int idCompany)
        {
            if (name != String.Empty)
            {
                if (_repo.IsJobExist(name, idCompany))
                {
                    return Json(new { IsExist = true, Message = "Takie stanowisko już istnieje!" });
                }
                var job = new Job {Name = name, IdCompany = idCompany};
                _repo.AddJob(job);
                _repo.Save();

                var lstJobs = _repo.GetJobsByCompany(idCompany).ToSelectListItem(0, j => j.IdJob, j => j.Name, j => j.Name, j => j.IdJob.ToString(CultureInfo.InvariantCulture));
                return Json(new { IsExist = false, lstJob = lstJobs.ToArray() });
            }
            return Json(new { IsExist = true, Message = "Proszę wpisać nazwę stanowiska!" });
        }

        [HttpGet]
        public ActionResult AddJob(int idCompany)
        {
            var job = new CreateJob {IdCompany = idCompany};
            var comp = _repo.GetCompanyById(idCompany);
            ViewBag.CompanyName = comp.Name;
            return View(job);
        }

        [HttpPost]
        public ActionResult AddJob(int idCompany, FormCollection formValues)
        {
            if (ModelState.IsValid)
            {
                var job = new Job {Name = formValues["Name"], IdCompany = Convert.ToInt32(formValues["IdCompany"])};
                _repo.AddJob(job);
                _repo.Save();
                TempData["message"] = new MessageInfo("Stanowisko " + formValues["Name"] + " zostało dodane.");
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditJob(int idJob)
        {
            var job = _repo.GetJobById(idJob);
            return View(job);
        }

        [HttpPost]
        public ActionResult EditJob(int idJob, FormCollection formValues)
        {
            if (ModelState.IsValid)
            {
                var job = _repo.GetJobById(idJob);
                job.Name = formValues["Name"];
                job.IsActive = formValues["isActive"] == "true,false";
                _repo.Save();
                TempData["message"] = new MessageInfo("Stanowisko " + formValues["Name"] + " zostało zmienione.");
            }
            return RedirectToAction("Index");
        }

    }
}
