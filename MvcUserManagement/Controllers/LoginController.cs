﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MvcUserManagement.Models;

namespace MvcUserManagement.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        [HttpGet]
        public ActionResult LogOn()
        {
            var memb = (HSOLoginProvider.HSOMembershipUser)Membership.Providers["HSOMembershipProvider"];
            if (memb != null)
            {
                HSOLoginProvider.Membership.HSODomain[] d = memb.GetDomainList();
                ViewBag.DomainList = d.Select(item =>
                                              new SelectListItem { Selected = false, Text = item.DomainName, Value = item.DomainId.ToString(CultureInfo.InvariantCulture) });
            }
            return View();
        }


        [HttpPost]
        public ActionResult LogOn(string returnUrl)
        {
            try
            {
                var memb = (HSOLoginProvider.HSOMembershipUser)Membership.Providers["HSOMembershipProvider"];
                if (memb != null)
                {
                    HSOLoginProvider.Membership.HSODomain[] d = memb.GetDomainList();
                    ViewBag.DomainList = d.Select(item =>
                                                  new SelectListItem { Selected = false, Text = item.DomainName, Value = item.DomainId.ToString(CultureInfo.InvariantCulture) });
                }
                if (Membership.ValidateUser(Request["tbxLogin"] + "/" + Request["ddlDirectory"], Request["tbxPassword"]))
                {
                    if (memb != null)
                    {
                        var usr = memb.GetUser(Request["tbxLogin"] + "/" + Request["ddlDirectory"], Request["tbxPassword"]);
                        if (usr != null)
                        {
                            var ticket = new FormsAuthenticationTicket(usr.UserName, true, 20);
                            string hashCookies = FormsAuthentication.Encrypt(ticket);

                            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hashCookies); // Hashed ticket

                            // Add the cookie to the response, user browser

                            Response.Cookies.Add(cookie);

                            // Get the requested page fromTake the url

                            UserProfile profile = UserProfile.GetUserProfile(usr.UserName);
                            Session["UserName"] = profile.Nazwisko + " " + profile.Imie;

                            if (Url.IsLocalUrl(returnUrl) && returnUrl != @"/Login/LogOff")
                            {
                                return Redirect(returnUrl);
                            }
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    ModelState.AddModelError("", "Błędna próba logowania. Login lub hasło są niepoprawne.");
                }
                else
                {
                    ModelState.AddModelError("", "Błędna próba logowania. Login lub hasło są niepoprawne.");
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("",
                                         ex.Message == "Nieprawidłowy użytkownik"
                                             ? "Błędna próba logowania. Login lub hasło są niepoprawne."
                                             : "Błędna próba logowania. Proszę wybrać firmę.");
            }
            
            return View();
        }

        [HttpGet]
        public void LogOff()
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }

    }
}
