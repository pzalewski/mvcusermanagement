﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using MvcUserManagement.Models;

namespace MvcUserManagement.Controllers
{
    public class RegionController : Controller
    {
        readonly UserRepository _repo = new UserRepository();
        //
        // GET: /Region/

        public ActionResult Index()
        {
            if (TempData["message"] != null)
            {
                var info = TempData["message"] as MessageInfo;
                if (info != null) ViewBag.Message = info.Message;
            }
            IEnumerable<SelectListItem> lstCompanies = _repo.FindAllCompanies().ToSelectListItem(1, c => c.IdCompany, c => c.Name, c => c.Name, c => c.IdCompany.ToString(CultureInfo.InvariantCulture));
            ViewBag.lstCompanies = lstCompanies;
            return View();
        }

        [HttpPost]
        public ActionResult GetRegionsList(string name, int idCompany, int toTake)
        {
            var lst = _repo.GetRegionsByNameCompany(name, idCompany, 0, toTake);
            int count = _repo.GetCountOfRegions(name, idCompany);

            int paging = 1;
            if (toTake > 0)
            {
                paging = count / toTake;
                if (count % toTake != 0)
                    paging++;
            }

            return Json(new { regionList = lst.ToArray(), count = paging });
        }

        [HttpPost]
        public ActionResult GetNextPage(string name, int idCompany, int toTake, int page)
        {

            var lst = _repo.GetRegionsByNameCompany(name, idCompany, toTake * (page - 1), toTake);
            int count = _repo.GetCountOfRegions(name, idCompany);
            int paging = count / toTake;
            if (count % toTake != 0)
                paging++;
            return Json(new { regionList = lst.ToArray(), count = paging });
        }

        public ActionResult IsRegionExist(string name, int idCompany)
        {
            return Json(!_repo.IsRegionExist(name, idCompany), JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsRegionExistBeyondId(string name, int idCompany, int idRegion)
        {
            return Json(!_repo.IsRegionExistBeyondId(name, idCompany, idRegion), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddJQueryRegion(string name, int idCompany)
        {
            if (name != String.Empty)
            {
                if (_repo.IsRegionExist(name, idCompany))
                {
                    return Json(new { IsExist = true, Message = "Taki region już istnieje!" });
                }
                var region = new Region {Name = name, IdCompany = idCompany};
                //ent.Jobs.AddObject(job);
                //ent.SaveChanges();
                _repo.AddRegion(region);
                _repo.Save();

                var lstRegions = _repo.GetRegionsByCompany(idCompany).ToSelectListItem(0, r => r.IdRegion, r => r.Name, r => r.Name, r => r.IdRegion.ToString(CultureInfo.InvariantCulture));
                return Json(new { IsExist = false, lstRegion = lstRegions.ToArray() });
            }
            return Json(new { IsExist = true, Message = "Proszę wpisać nazwę regionu!" });
        }

        [HttpGet]
        public ActionResult AddRegion(int idCompany)
        {
            var region = new CreateRegion {IdCompany = idCompany};
            var comp = _repo.GetCompanyById(idCompany);
            ViewBag.CompanyName = comp.Name;
            return View(region);
        }

        [HttpPost]
        public ActionResult AddRegion(int idCompany, FormCollection formValues)
        {
            if (ModelState.IsValid)
            {
                var region = new Region
                                    {Name = formValues["Name"], IdCompany = Convert.ToInt32(formValues["IdCompany"])};
                _repo.AddRegion(region);
                _repo.Save();
                TempData["message"] = new MessageInfo("Region " + formValues["Name"] + " został dodany.");
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditRegion(int idRegion)
        {
            var region = _repo.GetRegionById(idRegion);
            var comp = _repo.GetCompanyById(region.IdCompany);
            ViewBag.CompanyName = comp.Name;
            return View(region);
        }

        [HttpPost]
        public ActionResult EditRegion(int idRegion, FormCollection formValues)
        {
            if (ModelState.IsValid)
            {
                var region = _repo.GetRegionById(idRegion);
                region.Name = formValues["Name"];
                region.IsActive = formValues["isActive"] == "true,false";
                _repo.Save();
                TempData["message"] = new MessageInfo("Region " + formValues["Name"] + " został zmieniony.");
            }
            return RedirectToAction("Index");
        }

    }
}
