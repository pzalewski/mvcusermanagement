﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MvcUserManagement.Models
{
    [MetadataType(typeof(BranchValidation))]
    public partial class Branch
    {

    }

    public class BranchValidation
    {
        [Display(Name = "Nazwa oddziału")]
        [Required(ErrorMessage = "Wprowadź nazwę oddziału")]
        [Remote("IsBranchNameExistBeyondId", "Branch", HttpMethod = "Post", AdditionalFields = "IdBranch,IdCompany", ErrorMessage = "Taki oddział już istnieje")]
        public int Name { get; set; }

        [Display(Name = "Numer oddziału")]
        [Required(ErrorMessage = "Wprowadź numer oddziału")]
        [RegularExpression("\\d+", ErrorMessage = "Proszę wprowadzić poprawny numer oddziału")]
        [Remote("IsBranchNoExistBeyondId", "Branch", HttpMethod = "Post", AdditionalFields = "IdBranch,IdCompany", ErrorMessage = "Taki numer oddziału już istnieje")]
        public int NoBranch { get; set; }

        [Display(Name = "Data utwrzenia")]
        public DateTime CreateDate { get; set; }
        [Display(Name = "Data ostatniej zmiany")]
        public DateTime ChangeDate { get; set; }
        [Display(Name = "Czy aktywne")]
        public bool IsActive { get; set; }
        [Display(Name = "Region")]
        public int IdRegion { get; set; }
        [Display(Name = "Czy PK?")]
        public bool IsPK { get; set; }
        [Display(Name = "Oddział nadrzędny")]
        public int IdParent { get; set; }
    }

    [MetadataType(typeof(CreateBranchValidation))]
    public class CreateBranch : Branch
    {

    }

    public class CreateBranchValidation
    {
        [Display(Name = "Nazwa stanowiska")]
        [Required(ErrorMessage = "Wprowadź nazwę oddziału")]
        [Remote("IsBranchNameExist", "Branch", HttpMethod = "Post", AdditionalFields = "IdCompany", ErrorMessage = "Taki oddział już istnieje")]
        public int Name { get; set; }

        [Display(Name = "Numer oddziału")]
        [Required(ErrorMessage = "Wprowadź numer oddziału")]
        [RegularExpression("\\d+", ErrorMessage = "Proszę wprowadzić poprawny numer oddziału")]
        [Remote("IsBranchNoExist", "Branch", HttpMethod = "Post", AdditionalFields = "IdCompany", ErrorMessage = "Taki numer oddziału już istnieje")]
        public int NoBranch { get; set; }

        [Display(Name = "Data utwrzenia")]
        public DateTime CreateDate { get; set; }
        [Display(Name = "Data ostatniej zmiany")]
        public DateTime ChangeDate { get; set; }
        [Display(Name = "Czy aktywne")]
        public bool IsActive { get; set; }
        [Display(Name = "Region")]
        public int IdRegion { get; set; }
        [Display(Name = "Czy PK?")]
        public bool IsPK { get; set; }
        [Display(Name="Oddział nadrzędny")]
        public int IdParent { get; set; }

    }
}