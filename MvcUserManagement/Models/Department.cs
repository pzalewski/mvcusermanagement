﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MvcUserManagement.Models
{

    [MetadataType(typeof(DepartmentValidation))]
    public partial class Department
    {
    }

    public class DepartmentValidation
    {
        [Display(Name = "Nazwa działu")]
        [Required(ErrorMessage = "Wprowadź nazwę działu")]
        [Remote("IsDepartmentExistBeyondId", "Department", HttpMethod = "Post", AdditionalFields = "IdDepartment,IdCompany", ErrorMessage = "Taka jednostka organizacyjna już istnieje")]
        public int Name { get; set; }

        [Display(Name = "Data utwrzenia")]
        public DateTime CreateDate { get; set; }
        [Display(Name = "Data ostatniej zmiany")]
        public DateTime ChangeDate { get; set; }
        [Display(Name = "Czy aktywne")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(CreateDepartmentValidation))]
    public class CreateDepartment : Department
    {

    }

    public class CreateDepartmentValidation
    {
        [Display(Name = "Nazwa działu")]
        [Required(ErrorMessage = "Wprowadź nazwę działu")]
        [Remote("IsDepartmentExist", "Department", HttpMethod = "Post", AdditionalFields = "IdCompany", ErrorMessage = "Taka jednostka organizacyjna już istnieje")]
        public int Name { get; set; }

        [Display(Name = "Data utwrzenia")]
        public DateTime CreateDate { get; set; }
        [Display(Name = "Data ostatniej zmiany")]
        public DateTime ChangeDate { get; set; }
        [Display(Name = "Czy aktywne")]
        public bool IsActive { get; set; }
    }
}