﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MvcUserManagement.Models
{
    [MetadataType(typeof (JobValidation))]
    
    public partial class Job
    {

    }

    public class JobValidation
    {
        [Display(Name="Nazwa stanowiska")]
        [Required(ErrorMessage="Wprowadź nazwę stanowiska")]
        [Remote("IsJobExistBeyondId", "Job", HttpMethod = "Post", AdditionalFields = "IdJob,IdCompany", ErrorMessage = "Takie stanowisko już istnieje")]
        public int Name { get; set; }

        [Display(Name="Data utwrzenia")]
        public DateTime CreateDate { get; set; }
        [Display(Name = "Data ostatniej zmiany")]
        public DateTime ChangeDate { get; set; }
        [Display(Name = "Czy aktywne")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(CreateJobValidation))]
    public class CreateJob : Job
    { 
    
    }

    public class CreateJobValidation
    {
        [Display(Name = "Nazwa stanowiska")]
        [Required(ErrorMessage = "Wprowadź nazwę stanowiska")]
        [Remote("IsJobExist", "Job", HttpMethod = "Post", AdditionalFields = "IdCompany", ErrorMessage = "Takie stanowisko już istnieje")]
        public int Name { get; set; }
    }
}