﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MvcUserManagement.Models
{
    [MetadataType(typeof(RegionValidation))]
    public partial class Region
    {
    }

    public class RegionValidation
    {
        [Display(Name = "Nazwa regionu")]
        [Required(ErrorMessage = "Wprowadź nazwę regionu")]
        [Remote("IsRegionExistBeyondId", "Region", HttpMethod = "Post", AdditionalFields = "IdRegion,IdCompany", ErrorMessage = "Taki region już istnieje")]
        public int Name { get; set; }

        [Display(Name = "Data utwrzenia")]
        public DateTime CreateDate { get; set; }
        [Display(Name = "Data ostatniej zmiany")]
        public DateTime ChangeDate { get; set; }
        [Display(Name = "Czy aktywne")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(CreateRegionValidation))]
    public class CreateRegion : Region
    {

    }

    public class CreateRegionValidation
    {
        [Display(Name = "Nazwa regionu")]
        [Required(ErrorMessage = "Wprowadź nazwę regionu")]
        [Remote("IsRegionExist", "Region", HttpMethod = "Post", AdditionalFields = "IdCompany", ErrorMessage = "Taki region już istnieje")]
        public int Name { get; set; }
    }
}