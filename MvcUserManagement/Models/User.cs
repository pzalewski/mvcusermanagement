﻿using System.ComponentModel.DataAnnotations;
using MvcUserManagement.AOP;

namespace MvcUserManagement.Models
{
    [MetadataType(typeof (UserValidation))]
    public partial class User
    {
    }

    public class UserValidation
    {
        [Display(Name="Pager")]
        [Required]
        public int IdUser { get; set; }

        [Display(Name = "Imię")]
        [Required(ErrorMessage="Proszę wprowadzić imię użytkownika")]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        [Required(ErrorMessage = "Proszę wprowadzić nazwisko użytkownika")]
        public string LastName { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress, ErrorMessage="Podany adres email jest niepoprawny")]
        public string Email { get; set; }

        [Display(Name = "Czy aktywny")]
        public string IsActive { get; set; }
    }
}