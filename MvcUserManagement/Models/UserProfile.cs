﻿using System;
using System.Web.Profile;
using System.Web.Security;

namespace MvcUserManagement.Models
{
    public class UserProfile : ProfileBase
    {
        public static UserProfile GetUserProfile(string username)
        {
            return Create(username) as UserProfile;
        }
        public static UserProfile GetUserProfile()
        {
            var membershipUser = Membership.GetUser();
            if (membershipUser != null) return Create(membershipUser.UserName) as UserProfile;
            return null;
        }

        [SettingsAllowAnonymous(false)]
        public string Nazwisko
        {
            get { return base["Nazwisko"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string Imie
        {
            get { return base["Imie"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string Stanowisko
        {
            get { return base["Stanowisko"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string Dzial
        {
            get { return base["Dzial"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string Oddzial
        {
            get { return base["Oddzial"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public int NrOddzialu
        {
            get { return Convert.ToInt32(base["NrOddzialu"]); }
        }

        [SettingsAllowAnonymous(false)]
        public string NazwiskoKierownika
        {
            get { return base["NazwiskoKierownika"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string ImieKierownika
        {
            get { return base["ImieKierownika"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public int IdKierownika
        {
            get { return Convert.ToInt32(base["IdKierownika"]); }
        }

        [SettingsAllowAnonymous(false)]
        public string Region
        {
            get { return base["Region"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string Firma
        {
            get { return base["Firma"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string FirmaAdres
        {
            get { return base["FirmaAdres"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string Suffix
        {
            get { return base["Suffix"] as string; }
        }
    }
}
