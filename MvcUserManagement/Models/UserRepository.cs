﻿using System;
using System.Linq;
using MvcUserManagement.ViewModel;

namespace MvcUserManagement.Models
{
    public class UserRepository
    {
        private readonly UzytkownicyEntities _entities = new UzytkownicyEntities();

        #region Query

        public IQueryable<Company> FindAllCompanies()
        {
            return _entities.Companies;
        }

        public IQueryable<Application> FindAllAplications()
        {
            return _entities.Applications;
        }

        public IQueryable<Role> GetAllRolesByApp(int idApplication)
        {
            return _entities.Roles.Where(r => r.IdApplication == idApplication).OrderBy(r => r.Name);
        }

        public IOrderedQueryable<UserRole> GetAllRolesByUser(int idUser)
        {
            return _entities.UserRoles.Where(ur => ur.IdUser == idUser).OrderBy(ur => ur.Role.Application.Name).ThenBy(ur => ur.Role.Name);
        }

        public Company GetCompanyById(int idCompany)
        {
            return _entities.Companies.FirstOrDefault(c => c.IdCompany == idCompany);
        }

        public int GetCountOfUsers(string lastName, int idCompany)
        {
            return _entities.Users.Where(u => u.IdCompany == idCompany).Count(u => u.LastName.Contains(lastName));
        }

        public int GetCountOfBranches(string name, int idCompany)
        {
            return _entities.Branches.Where(b => b.IdCompany == idCompany).Count(b => b.Name.Contains(name));
        }

        public int GetCountOfJobs(string name, int idCompany)
        {
            return _entities.Jobs.Where(j => j.IdCompany == idCompany).Count(j => j.Name.Contains(name));
        }

        public int GetCountOfDepartments(string name, int idCompany)
        {
            return _entities.Departments.Where(d => d.IdCompany == idCompany).Count(d => d.Name.Contains(name));
        }

        public int GetCountOfRegions(string name, int idCompany)
        {
            return _entities.Regions.Where(r => r.IdCompany == idCompany).Count(r => r.Name.Contains(name));
        }

        public IQueryable<UserViewModel> GetUserByLNameCompany(string lastName, int idCompany, int fromTake, int toTake)
        {
            if (toTake != 0)
            {
                IQueryable<UserViewModel> test = (_entities.Users.Where(
                    usr => usr.LastName.Contains(lastName) && usr.IdCompany == idCompany).OrderBy(usr => usr.LastName).
                    Select(usr => new UserViewModel
                                      {
                                          Id = usr.IdUser,
                                          LastName = usr.LastName,
                                          FirstName = usr.FirstName,
                                          Job = usr.Job.Name,
                                          IdJob = usr.IdJob,
                                          ManagerFirstName = usr.Manager.FirstName,
                                          ManagerLastName = usr.Manager.LastName,
                                          IdManager = usr.IdManager,
                                          Branch = usr.Branch.Name,
                                          NoBranch = usr.Branch.NoBranch,
                                          IdBranch = usr.IdBranch,
                                          Department = usr.Department.Name,
                                          IdDepartment = usr.IdDepartment,
                                          IsActive = usr.IsActive
                                      })).Skip(fromTake).Take(toTake);
                return test;
            }
            else
            {
                IQueryable<UserViewModel> test = (_entities.Users.Where(usr => usr.LastName.Contains(lastName) && usr.IdCompany == idCompany).
                    OrderBy(usr => usr.LastName).Select(usr => new UserViewModel
                                                                   {
                                                                       Id = usr.IdUser,
                                                                       LastName = usr.LastName,
                                                                       FirstName = usr.FirstName,
                                                                       Job = usr.Job.Name,
                                                                       IdJob = usr.IdJob,
                                                                       ManagerFirstName = usr.Manager.FirstName,
                                                                       ManagerLastName = usr.Manager.LastName,
                                                                       IdManager = usr.IdManager,
                                                                       Branch = usr.Branch.Name,
                                                                       NoBranch = usr.Branch.NoBranch,
                                                                       IdBranch = usr.IdBranch,
                                                                       Department = usr.Department.Name,
                                                                       IdDepartment = usr.IdDepartment,
                                                                       IsActive = usr.IsActive
                                                                   })).Skip(fromTake);
                return test;
            }
        }

        public IQueryable<BranchViewModel> GetBranchesByNameCompany(string name, int idCompany, int fromTake, int toTake)
        {
            if (toTake != 0)
            {
                IQueryable<BranchViewModel> test = (_entities.Branches.Where(
                    branch => branch.Name.Contains(name) && branch.IdCompany == idCompany).OrderBy(
                        branch => branch.Name).Select(branch => new BranchViewModel
                                                                    {
                                                                        IdBranch = branch.IdBranch,
                                                                        Name = branch.Name,
                                                                        NoBranch = branch.NoBranch,
                                                                        RegionName = branch.Region.Name,
                                                                        IsPK = branch.IsPK,
                                                                        ParentName = branch.ParentBranch.Name,
                                                                        IsActive = branch.IsActive
                                                                    })).Skip(fromTake).Take(toTake);
                return test;
            }
            else
            {
                IQueryable<BranchViewModel> test = (_entities.Branches.Where(branch => branch.Name.Contains(name) && branch.IdCompany == idCompany)
                    .OrderBy(branch => branch.Name).Select(branch => new BranchViewModel
                                                                         {
                                                                             IdBranch = branch.IdBranch,
                                                                             Name = branch.Name,
                                                                             NoBranch = branch.NoBranch,
                                                                             RegionName = branch.Region.Name,
                                                                             IsPK = branch.IsPK,
                                                                             ParentName = branch.ParentBranch.Name,
                                                                             IsActive = branch.IsActive
                                                                         })).Skip(fromTake);
                return test;
            }
        }

        public IQueryable<JobViewModel> GetJobsByNameCompany(string name, int idCompany, int fromTake, int toTake)
        {
            if (toTake != 0)
            {
                IQueryable<JobViewModel> test = (_entities.Jobs.Where(job => job.Name.Contains(name) && job.IdCompany == idCompany).OrderBy(
                    job => job.Name).Select(job => new JobViewModel
                                                       {
                                                           IdJob = job.IdJob,
                                                           Name = job.Name,
                                                           IsActive = job.IsActive
                                                       })).Skip(fromTake).Take(toTake);
                return test;
            }
            else
            {
                IQueryable<JobViewModel> test = (_entities.Jobs.Where(
                    job => job.Name.Contains(name) && job.IdCompany == idCompany).OrderBy(job => job.Name).Select(
                        job => new JobViewModel
                                   {
                                       IdJob = job.IdJob,
                                       Name = job.Name,
                                       IsActive = job.IsActive
                                   })).Skip(fromTake);
                return test;
            }
        }

        public IQueryable<DepartmentViewModel> GetDepartmentsByNameCompany(string name, int idCompany, int fromTake, int toTake)
        {
            if (toTake != 0)
            {
                IQueryable<DepartmentViewModel> test = (_entities.Departments.Where(
                    department => department.Name.Contains(name) && department.IdCompany == idCompany).OrderBy(
                        department => department.Name).Select(
                            department => new DepartmentViewModel
                                              {
                                                  IdDepartment = department.IdDepartment,
                                                  Name = department.Name,
                                                  IsActive = department.IsActive
                                              })).Skip(fromTake).Take(toTake);
                return test;
            }
            else
            {
                IQueryable<DepartmentViewModel> test = (_entities.Jobs.Where(job => job.Name.Contains(name) && job.IdCompany == idCompany).OrderBy(
                    job => job.Name).Select(job => new DepartmentViewModel
                                                       {
                                                           IdDepartment = job.IdJob,
                                                           Name = job.Name,
                                                           IsActive = job.IsActive
                                                       })).Skip(fromTake);
                return test;
            }
        }

        public IQueryable<RegionViewModel> GetRegionsByNameCompany(string name, int idCompany, int fromTake, int toTake)
        {
            if (toTake != 0)
            {
                IQueryable<RegionViewModel> test = (_entities.Regions.Where(region => region.Name.Contains(name) && region.IdCompany == idCompany)
                    .OrderBy(region => region.Name).Select(region => new RegionViewModel
                                                                         {
                                                                             IdRegion = region.IdRegion,
                                                                             Name = region.Name,
                                                                             IsActive = region.IsActive
                                                                         })).Skip(fromTake).Take(toTake);
                return test;
            }
            else
            {
                IQueryable<RegionViewModel> test = (_entities.Regions.Where(region => region.Name.Contains(name) && region.IdCompany == idCompany)
                    .OrderBy(region => region.Name).Select(region => new RegionViewModel
                                                                         {
                                                                             IdRegion = region.IdRegion,
                                                                             Name = region.Name,
                                                                             IsActive = region.IsActive
                                                                         })).Skip(fromTake);
                return test;
            }
        }

        public User GetUserById(int idUser)
        {
            return _entities.Users.FirstOrDefault(u => u.IdUser == idUser);
        }
        
        public Branch GetBranchById(int idBranch)
        {
            return _entities.Branches.FirstOrDefault(b => b.IdBranch == idBranch);
        }

        public Department GetDepartmentById(int idDepartment)
        {
            return _entities.Departments.FirstOrDefault(d => d.IdDepartment == idDepartment);
        }

        public Job GetJobById(int idJob)
        {
            return _entities.Jobs.FirstOrDefault(j => j.IdJob == idJob);
        }

        public Region GetRegionById(int idRegion)
        {
            return _entities.Regions.FirstOrDefault(r => r.IdRegion == idRegion);
        }

        public IQueryable<Job> GetJobsByCompany(int idCompany)
        {
            return _entities.Jobs.Where(j => j.IdCompany == idCompany).OrderBy(j => j.Name);
        }

        public bool IsUserInRole(int idUser, int idRole)
        {
            return _entities.UserRoles.Any(u => u.IdUser == idUser && u.IdRole == idRole);
        }

        public bool IsImported(int idCompany)
        {
            var firstOrDefault = _entities.Companies.FirstOrDefault(c => c.IdCompany == idCompany);
            return firstOrDefault != null && firstOrDefault.DGAImport;
        }

        public IQueryable<Branch> GetBranchesByCompany(int idCompany)
        {
            return _entities.Branches.Where(b => b.IdCompany == idCompany).OrderBy(b => b.Name);
        }

        public IQueryable<Department> GetDepartmentsByCompany(int idCompany)
        {
            return _entities.Departments.Where(d => d.IdCompany == idCompany).OrderBy(d => d.Name);
        }

        public IQueryable<User> GetManagersByCompany(int idCompany)
        {
            return _entities.Users.Where(u => u.IdCompany == idCompany).OrderBy(u => u.LastName).ThenBy(u => u.FirstName);
        }

        public IQueryable<Region> GetRegionsByCompany(int idCompany)
        {
            return _entities.Regions.Where(r => r.IdCompany == idCompany).OrderBy(r => r.Name);
        }
        #endregion

        #region WALIDACJA (REMOTE)

        public bool IsBranchNameExist(string name, int idCompany)
        {
            return _entities.Branches.Any(b => b.Name.ToLower() == name.ToLower() && b.IdCompany == idCompany);
        }

        public bool IsBranchNoExist(int noBranch, int idCompany)
        {
            return _entities.Branches.Any(b => b.NoBranch == noBranch && b.IdCompany == idCompany);
        }

        public bool IsBranchNameExistBeyondId(string name, int idCompany, int idBranch)
        {
            return _entities.Branches.Any(b => b.Name.ToLower() == name.ToLower() && b.IdCompany == idCompany && b.IdBranch != idBranch);
        }

        public bool IsBranchNoExistBeyondId(int noBranch, int idCompany, int idBranch)
        {
            return _entities.Branches.Any(b => b.NoBranch == noBranch && b.IdCompany == idCompany && b.IdBranch != idBranch);
        }

        public bool IsBranchExist(string name, int noBranch, int idCompany)
        {
            return (from branch in _entities.Branches
                    where (branch.NoBranch == noBranch || branch.Name.ToLower() == name.ToLower()) && branch.IdCompany == idCompany
                    select branch).Any();
        }

        public bool IsDepartmentExist(string name, int idCompany)
        {
            return _entities.Departments.Any(d => d.Name.ToLower() == name.ToLower() && d.IdCompany == idCompany);
        }

        public bool IsDepartmentExistBeyondId(string name, int idCompany, int idDepartment)
        {
            return _entities.Departments.Any(d => d.Name.ToLower() == name.ToLower() && d.IdCompany == idCompany && d.IdDepartment != idDepartment);
        }

        public bool IsJobExist(string name, int idCompany)
        {
            return _entities.Jobs.Any(j => j.Name.ToLower() == name.ToLower() && j.IdCompany == idCompany);
        }

        public bool IsJobExistBeyondId(string name, int idCompany, int idJob)
        {
            return _entities.Jobs.Any(j => j.Name.ToLower() == name.ToLower() && j.IdCompany == idCompany && j.IdJob != idJob);
        }

        public bool IsRegionExist(string name, int idCompany)
        {
            return _entities.Regions.Any(r => r.Name.ToLower() == name.ToLower() && r.IdCompany == idCompany);
        }

        public bool IsRegionExistBeyondId(string name, int idCompany, int idRegion)
        {
            return _entities.Regions.Any(r => r.Name.ToLower() == name.ToLower() && r.IdCompany == idCompany && r.IdRegion != idRegion);
        }

        #endregion

        public void AddJob(Job job)
        {
            _entities.Jobs.AddObject(job);
        }

        public void AddBranch(Branch branch)
        {
            _entities.Branches.AddObject(branch);
        }

        public void AddDepartment(Department department)
        {
            _entities.Departments.AddObject(department);
        }

        public void AddRegion(Region region)
        {
            _entities.Regions.AddObject(region);
        }

        public void AddUser(User user)
        {
            _entities.Users.AddObject(user);
        }

        public void AddUserRole(UserRole role)
        {
            _entities.UserRoles.AddObject(role);
        }

        public void DeleteUserRole(int idUserRole)
        {
            _entities.UserRoles.DeleteObject(_entities.UserRoles.FirstOrDefault(r => r.IdUserRole == idUserRole));
        }

        //Persistence
        public void Save()
        {
            try
            {
                _entities.SaveChanges();
            }
            catch (Exception)
            { }

        }

    }
}