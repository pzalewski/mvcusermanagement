﻿function messageBox(msg) {
    $.blockUI({
        message: msg,
        fadeIn: 700,
        fadeOut: 700,
        timeout: 2500,
        showOverlay: false,
        centerY: false,
        css: {
            width: '350px',
            border: 'none',
            padding: '25px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .8,
            color: '#fff'
        }
    });
}

function dialogBox(msg) {
    $.blockUI({
        message: msg,
        fadeIn: 700,
        fadeOut: 700,
        showOverlay: false,
        centerY: false,
        css: {
            border: 'none',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .8,
            color: '#fff'
        }
    });
}
