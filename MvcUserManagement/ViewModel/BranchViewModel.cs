﻿namespace MvcUserManagement.ViewModel
{
    public class BranchViewModel
    {
        public int IdBranch { get; set; }
        public string Name { get; set; }
        public int? NoBranch { get; set; }
        public string RegionName { get; set; }
        public bool? IsPK { get; set; }
        public string ParentName { get; set; }
        public bool? IsActive { get; set; }
    }
}