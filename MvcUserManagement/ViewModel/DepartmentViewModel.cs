﻿namespace MvcUserManagement.ViewModel
{
    public class DepartmentViewModel
    {
        public int IdDepartment { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
    }
}