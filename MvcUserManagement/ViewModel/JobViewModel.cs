﻿namespace MvcUserManagement.ViewModel
{
    public class JobViewModel
    {
        public int IdJob { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
    }
}