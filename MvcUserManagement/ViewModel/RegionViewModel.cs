﻿namespace MvcUserManagement.ViewModel
{
    public class RegionViewModel
    {
        public int IdRegion { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
    }
}