﻿namespace MvcUserManagement.ViewModel
{
    //public class UserViewModel
    //{
    //    private int Id;
    //    private string Nazwisko;
    //    private string Imie;
    //    private string Stanowisko;
    //    private int StanowiskoId;
    //    private string KierownikImie;
    //    private string KierownikNazwisko;
    //    private int KierownikId;
    //    private string Oddzial;
    //    private int OddzialNr;
    //    private int OddzialId;
    //    private string Dzial;
    //    private int DzialId;
    //    private string Region;
    //    private int RegionId;
    //    private bool Aktywny;

    //    public UserViewModel(MvcUserManagement.Models.User _usr)
    //    {
    //        Id = _usr.ID;
    //        Nazwisko = _usr.Nazwisko;
    //        Imie = _usr.Imie;
    //        if (_usr.Stanowiska != null)
    //        {
    //            Stanowisko = _usr.Stanowiska.Nazwa;
    //            StanowiskoId = _usr.Stanowiska.Id;
    //        }
    //    }
    //}

    public class UserViewModel
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Job { get; set; }
        public int? IdJob { get; set; }
        public string ManagerFirstName { get; set; }
        public string ManagerLastName { get; set; }
        public int? IdManager { get; set; }
        public string Branch { get; set; }
        public int? NoBranch { get; set; }
        public int? IdBranch { get; set; }
        public string Department { get; set; }
        public int? IdDepartment { get; set; }
        public string Region { get; set; }
        public int? IdRegion { get; set; }
        public bool IsActive { get; set; }
    }

    //public class UserViewModelList
    //{
    //    private List<UserViewModel> lst;

    //    public UserViewModelList(IQueryable<MvcUserManagement.Models.User> _usrLst)
    //    {
    //        lst = new List<UserViewModel>();
    //        foreach (MvcUserManagement.Models.User u in _usrLst)
    //        {
    //            UserViewModel usr = new UserViewModel(u);
    //            lst.Add(usr);
    //        }
    //    }
    //}
}